from django.urls import path
from . import views


urlpatterns = [
    
    path('', views.get_index_page, name="home"),
    path('c1/<int:course_id>/', views.get_one_course_page),
    path('course/list', views.get_list_pages, name="course"),
    path('course/learn/<int:course_id>', views.lecture_learn, name="lecture_learn_data"),
    path('course/learn_practice/<int:course_id>', views.practice_learn, name="practice_learn_data"),
    path('c1_list', views.Course__page),
]

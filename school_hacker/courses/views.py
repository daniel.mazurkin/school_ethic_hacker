from django.views import generic
from courses.models import Course
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import Http404
from applications.models import ApplicationCourse
from lectures.models import LecturePage
from tasks.models import PracticeTask
from courses.utils import checked_right_user


class CourseDetailView(generic.DetailView):
    model = Course

    def get_context_data(self, **kwargs):
        kwargs.setdefault("view", self)
        if self.extra_context is not None:
            kwargs.update(self.extra_context)

        application_course_by_user = ApplicationCourse.course_by_data(
            kwargs['object'], self.request.user
        )

        kwargs['is_exist_user_in_current_course'] = False
        kwargs['is_exist_user_in_current_course'] = False

        if application_course_by_user:
            kwargs['is_exist_user_in_current_course'] = True
            kwargs['is_success_payment'] = application_course_by_user.is_success_payment

        return kwargs


def get_index_page(request):
    courses = Course.objects.all()
    return render(request, 'page/index.html', {
        "courses": courses
    })


@login_required
def get_one_course_page(request, course_id: int):
    course = Course.objects.filter(id=course_id).first()
    if not course:
        raise Http404

    return render(request, 'page/courses_1.html', {"course": course})
    

@login_required
def get_list_pages(request):
    application_courses = []
    if isinstance(request.user.id, int):
        application_courses = ApplicationCourse.objects.filter(
            user=request.user,
            is_success_payment=True
        )
    courses_list = []
    for app in application_courses:
        courses_list.append(app.course)
    return render(request, 'page/course_list.html', {"courses_list": courses_list})


@login_required
def Course__page(request):
    return render(request, 'page/course.html', {})


@login_required
def lecture_learn(request, course_id):
    message_for_error = checked_right_user(request.user, course_id)
    lectures = LecturePage.objects.filter(course__pk=course_id)
    return render(request, 'page/lecture_learn.html',
                  {
                        "message_error": message_for_error,
                        "lectures": lectures,
                        "course_id": course_id
                   })


@login_required
def practice_learn(request, course_id):
    message_for_error = checked_right_user(request.user, course_id)
    practices = PracticeTask.objects.filter(course__pk=course_id)
    return render(request, 'page/practice_learn.html', {
        "message_error": message_for_error,
        "practices": practices,
        "course_id": course_id
    })
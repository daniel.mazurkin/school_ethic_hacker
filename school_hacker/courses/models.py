from django.db import models
from colorfield.fields import ColorField


class Course(models.Model):
    """Модель, которая описывает курс."""

    name = models.CharField(max_length=255, verbose_name="Наименование курса")
    short_description = models.CharField(max_length=512, verbose_name="Короткое описание")
    logo = models.ImageField(verbose_name='Логотип курса', null=True)
    cost = models.IntegerField(verbose_name="Стоимость регистрации")

    owner_competition = models.CharField(verbose_name="Обладатель компетенции", max_length=255,
                                         null=True,
                                         blank=True)

    describe_competition = models.CharField(verbose_name="Описание комптенеции", max_length=1024,
                                            null=True,
                                            blank=True)

    image_competition = models.ImageField(verbose_name="Изображение для компетенции",
                                          max_length=1024,
                                          null=True,
                                          blank=True)

    text_for_competition = models.CharField(verbose_name="Вспомогательное название комптенеции",
                                            help_text="Готовы начать свой путь в мире <переменная, которую вы зададите>",
                                            max_length=255,
                                            null=True,
                                            blank=True)

    cost_of_hour = models.IntegerField(verbose_name="Количество расчетных часов на курс")
    description = models.TextField(verbose_name="Описание курса")
    color = ColorField(default='#FF0000', verbose_name="Фон для логотипа курса")    
    
    def __str__(self):
        return f'№ {self.pk} {self.name}'

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'


class ProcessingCourse(models.Model):
    """Описание того как происходит обучение"""
    course = models.ForeignKey(
        Course,
        verbose_name="Курсы",
        on_delete=models.CASCADE
    )

    short_description = models.CharField(
        max_length=512,
        verbose_name="Короткое описание #1"
    )

    image_description = models.ImageField(
        verbose_name="Изображение #1"
    )

    short_description_second = models.CharField(
        max_length=512,
        verbose_name="Короткое описание #2"
    )

    image_description_second = models.ImageField(
        verbose_name="Изображение #2"
    )

    class Meta:
        verbose_name = "Как происходит обучение"
        verbose_name_plural = "Как происходит обучение"

from applications.models import ApplicationCourse


def checked_right_user(user, course_id) -> str:
    """Проверяет доступность пользователя к материалам."""
    application_course = ApplicationCourse.objects.filter(
        user=user, course__pk=course_id, is_success_payment=True,
    ).first()

    message_for_error = None
    if not application_course:
        message_for_error = "Произошла ошибка. Вам не доступен этот материал"

    return message_for_error
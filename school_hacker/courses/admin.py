from django.contrib import admin
from courses.models import Course, ProcessingCourse


class ProcessingCourseInline(admin.TabularInline):
    model = ProcessingCourse

    def get_extra(self, request, obj=None, **kwargs):
        """Hook for customizing the number of extra inline forms."""
        procc_course_count = 0
        if obj is not None:
            procc_course_count = obj.processingcourse_set.count()
        extra = None

        if procc_course_count == 1:
            extra = 0
        else:
            extra = 1

        return extra

    def has_add_permission(self, request, obj):
        procc_course_count = 0
        if obj is not None:
            procc_course_count = obj.processingcourse_set.count()
        has_add = None

        if procc_course_count == 1:
            has_add = False
        else:
            has_add = True

        return has_add


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    inlines = [ProcessingCourseInline]


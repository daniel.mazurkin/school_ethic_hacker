from django.urls import path
from lectures.views import lecture_read_info


urlpatterns = [
    path('lectures/<int:lecture_id>', lecture_read_info, name="lecture_read_info")
]


from django.contrib.auth.decorators import login_required
from lectures.models import LecturePage
from django.shortcuts import render


@login_required
def lecture_read_info(request, lecture_id):
    """Возвращает информацию о лекции."""
    lecture = LecturePage.objects.filter(pk=lecture_id).first()
    question_data = lecture.lecturepage.question_data
    list_of_questions = []

    if hasattr(question_data, 'raw_data') and question_data.raw_data is not None:
        try:
            answers = list(question_data.raw_data[1]['value'].values())[:-1]
            answers_list = []

            for index, answer in enumerate(answers):
                answers_list.append({
                    "number": index,
                    "text_answer": answer
                })

            list_of_questions.append({
                "text_question": question_data.raw_data[0]['value'],
                "answers": answers_list
            })
        except IndexError:
            ...

    return render(request, template_name='page/lecture_1.html', context={
        "request": request,
        "lecture": lecture,
        "test_questions": list_of_questions,
    })
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.admin.panels import FieldPanel
from courses.models import Course
from django.db import models


class LecturePage(Page):
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE,
        verbose_name='Курс', null=True
    )
    
    body = StreamField([
        ('heading', blocks.CharBlock(form_classname="title", max_length=255, help_text="Заголовок",
                                     verbose_name="Заголовок", null=True, blank=True)),
        ('paragraph', blocks.RichTextBlock(help_text="Текст", max_length=255, null=True, blank=True)),
        ('image', ImageChooserBlock(null=True, blank=True)),
        ('document', DocumentChooserBlock(null=True, blank=True)),
    ])

    question_data = StreamField([
        ('Question', blocks.CharBlock(form_classname="title", max_length=512,
                                      verbose_name="Вопрос",
                                      help_text="Вопрос после лекции", null=True)),
        ('Answers', blocks.StructBlock([
            ('first_answer', blocks.CharBlock(verbose_name="Первый ответ",
                                              help_text="Первый ответ теста")),
            ('second_answer', blocks.CharBlock(verbose_name="Второй ответ",
                                               help_text="Второй ответ теста")),
            ('third_answer', blocks.CharBlock(verbose_name="Третий ответ",
                                              help_text="Третий ответ теста")),
            ('fourth_answer', blocks.CharBlock(verbose_name="Четвертый ответ",
                                               help_text="Четвертый ответ теста")),
            ('number_correct', blocks.IntegerBlock(verbose_name="Номер правильного ответа",
                                                   help_text="Введите номер правильного ответа выше"))
        ])),

    ], null=True)

    content_panels = Page.content_panels + [
        FieldPanel('body'),
        FieldPanel('course'),
        FieldPanel('question_data')
    ]
    

    class Meta:
        verbose_name = "Страница с лекцией"
        verbose_name_plural = "Страницы с лекцией"

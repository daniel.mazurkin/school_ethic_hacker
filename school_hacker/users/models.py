from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class UserModel(AbstractUser):

    is_verify_email = models.BooleanField(
        verbose_name="Подтверженный email",
        default=False
    )

    phone = models.CharField(
        verbose_name="Телефон",
        max_length=20
    )

    image_avatar = models.ImageField(
        verbose_name="Аватарка",
        null=True,
        blank=True,
        default=None
    )

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

"""
URL configuration for school_hacker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from wagtail import urls as wagtail_urls
from django.contrib.auth.models import Group
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail.images.models import Image
from taggit.models import Tag
from wagtail.documents.models import Document
from django.contrib.sites.models import Site


admin.site.unregister(Site)
admin.site.unregister(Group)
admin.site.unregister(Tag)
admin.site.unregister(Document)
admin.site.unregister(Image)

urlpatterns = [
    path('django-admin/', admin.site.urls),
    path('lectures/', include("lectures.urls")),
    path('admin/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),
    path('courses/', include("courses.urls"), name="start_courses"),
    path('user/', include("profile_info.urls")),
    path("applications/", include("applications.urls")),
    path('djrichtextfield/', include('djrichtextfield.urls')),
    path('certificates/', include('certificates.urls')),
    path('auth/', include("login_app.urls")),
    path('answers', include('answers_users.urls')),
    path('achievements', include('achievements.urls')),
    re_path(r'', include(wagtail_urls)),
]

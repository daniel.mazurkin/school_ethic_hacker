from django.shortcuts import render, get_object_or_404
from .models import Certificate
from django.http import FileResponse
from django.contrib.auth.decorators import login_required


@login_required
def course_certificates(request):
    certificates = Certificate.objects.filter(user=request.user)
    return render(request, 'page/course_sert_list.html', {
        "certificates": certificates
    })


@login_required
def course_certificate_page(request, pk):
    """
    Представление для скачивания сертификата по ID.
    """
    certificate = get_object_or_404(Certificate, pk=pk)

    if request.method == 'GET':
        # Отдаем файл сертификата как FileResponse
        response = FileResponse(certificate.file.open())
        response['Content-Disposition'] = f'attachment; filename="{certificate.file.name}"'
        return response

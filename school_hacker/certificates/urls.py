from django.urls import path
from .views import course_certificates, course_certificate_page


urlpatterns = [
    path('', course_certificates, name="certificate_list"),
    path('<int:pk>/', course_certificate_page, name='certificate_one_get'),
]
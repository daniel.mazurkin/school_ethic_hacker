from django.db import models
from courses.models import Course
from users.models import UserModel


class Certificate(models.Model):
    course = models.ForeignKey(
        Course,
        verbose_name="Курс",
        on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        UserModel,
        verbose_name="Пользователь",
        on_delete=models.CASCADE
    )

    logo = models.ImageField(
        upload_to="uploads/% Y/% m/% d/",
        verbose_name="Логотип",
        null=True
    )

    file = models.FileField(
        upload_to='certificates/',
        verbose_name="Файл с сертификатом"
    )

    class Meta:
        verbose_name = "Сертификат"
        verbose_name_plural = "Сертификаты"

    def __str__(self):
        return f"Сертификат по курсу {self.course} для {self.user}"

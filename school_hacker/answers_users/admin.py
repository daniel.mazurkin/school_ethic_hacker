from django.contrib import admin
from answers_users.models import AnswerUserFile, AnswerUserText, AnswerTestLectures


@admin.register(AnswerTestLectures)
class AnswerTestLecturesAdmin(admin.ModelAdmin):
    list_display = ('user', 'lecture', )
    list_filter = ('user', )
    readonly_fields = ('number_answer', 'lecture', 'user', 'is_correct_answer')

    def has_add_permission(self, request):
        return False

@admin.register(AnswerUserFile)
class AnswerUserFileAdmin(admin.ModelAdmin):
    list_display = ('course', 'task')
    list_filter = ('course', )
    readonly_fields = ('course', 'task', 'user', 'file', )


@admin.register(AnswerUserText)
class AnswerUserTextAdmin(admin.ModelAdmin):
    list_display = ('course', 'task')
    readonly_fields = ('course', 'task', 'user', 'info_about_answers', )
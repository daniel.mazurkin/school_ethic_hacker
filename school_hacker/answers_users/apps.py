from django.apps import AppConfig


class AnswersUsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'answers_users'
    verbose_name = "Ответы студентов"
    verbose_name_plural = "Ответы студентов"
from django.db import models
from courses.models import Course
from tasks.models import PracticeTask
from users.models import UserModel
from lectures.models import LecturePage


class AnswerUserFile(models.Model):
    course = models.ForeignKey(
        Course,
        verbose_name="Курс",
        on_delete=models.SET_NULL,
        null=True
    )

    task = models.ForeignKey(
        PracticeTask,
        verbose_name="Задача",
        on_delete=models.SET_NULL,
        null=True
    )

    user = models.ForeignKey(
        UserModel,
        verbose_name="Пользователь",
        on_delete=models.SET_NULL,
        null=True
    )

    file = models.FileField(
        verbose_name="Файл с ответов",
    )

    def __str__(self):
        return f'{self.user} {self.course} {self.task}'

    class Meta:
        verbose_name = "Ответ с файлом"
        verbose_name_plural = "Ответы с файлом"


class AnswerUserText(models.Model):
    course = models.ForeignKey(
        Course,
        verbose_name="Курс",
        on_delete=models.SET_NULL,
        null=True
    )

    task = models.ForeignKey(
        PracticeTask,
        verbose_name="Задача",
        on_delete=models.SET_NULL,
        null=True
    )

    user = models.ForeignKey(
        UserModel,
        verbose_name="Пользователь",
        on_delete=models.SET_NULL,
        null=True
    )

    info_about_answers = models.CharField(
        max_length=1024,
        verbose_name="Ответы пользователя (через ;)",
    )

    def __str__(self):
        return f'{self.user} {self.course} {self.task}'

    class Meta:
        verbose_name = "Текстовые ответы"
        verbose_name_plural = "Текстовые ответы"


class AnswerTestLectures(models.Model):

    user = models.ForeignKey(
        UserModel, verbose_name="Пользователь",
        on_delete=models.CASCADE
    )

    lecture = models.ForeignKey(
        LecturePage, verbose_name="Лекция",
        on_delete=models.CASCADE
    )

    is_correct_answer = models.BooleanField(
        default=False, verbose_name="Правильный ли ответ пользователя"
    )

    number_answer = models.IntegerField(
        verbose_name="Номер ответа пользователя"
    )

    def __str__(self):
        return f"Пользователь: {self.user} | Номер ответа: {self.number_answer}"

    class Meta:
        verbose_name = "Ответ на тест после лекции"
        verbose_name_plural = "Ответы на тесты после лекции"

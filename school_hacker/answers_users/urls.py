from .views import answer_user_commit
from django.urls import path


urlpatterns = [
    path("/lectures/<int:lecture_id>/", answer_user_commit, name="answer_commit")
]
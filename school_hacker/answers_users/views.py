from django.contrib.auth.decorators import login_required
from answers_users.models import AnswerTestLectures
from lectures.models import LecturePage
from django.http import HttpResponseRedirect
from achievements.models import AchievementUser


def check_correct_answer_user(lecture: LecturePage, text_answer) -> bool:
    """Проверяет корректность введенного ответа от пользователя."""
    is_correct_number_answer = False
    try:
        answer_user = int(text_answer)
        number_correct_answer = lecture.question_data.raw_data[1]['value']['number_correct']
        if number_correct_answer == answer_user:
            is_correct_number_answer = True
    except IndexError:
        ...
    return is_correct_number_answer


def set_user_achievement(user, is_correct_answer):
    """Устанавливает достижение пользователя."""
    exists_answer = AnswerTestLectures.objects.filter(user=user).first()

    if exists_answer and is_correct_answer:
        achievement_user = AchievementUser(
            user=user,
            text="Вы ответили успешно на первый вопрос!"
        )
        achievement_user.save()

@login_required
def answer_user_commit(request, lecture_id):
    """Фиксирует ответы пользователя."""
    lecture = LecturePage.objects.filter(pk=lecture_id).first()
    is_correct_number_answer = check_correct_answer_user(lecture, request.POST['text_answer'])
    answer_test = AnswerTestLectures(
        user=request.user,
        lecture=lecture,
        number_answer=int(request.POST['text_answer']),
        is_correct_answer=is_correct_number_answer
    )
    answer_test.save()
    set_user_achievement(request.user, is_correct_number_answer)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


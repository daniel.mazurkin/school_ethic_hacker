from django.db import models

import lectures.models
from courses.models import Course
from modelcluster.fields import ParentalKey


class PracticeTask(models.Model):
    """Модель с практическим заданием."""
    course = models.ForeignKey(
        Course,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Курс"
    )

    name_practice = models.CharField(
        max_length=255,
        verbose_name="Имя практики"
    )

    lecture = models.ForeignKey(
        lectures.models.LecturePage,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="К какой лекции прикреплено"
    )

    def __str__(self):
        return f'{self.name_practice}'

    class Meta:
        verbose_name = "Практическое задание"
        verbose_name_plural = "Практические задания"


class QuestionTestTask(models.Model):
    """Текст вопроса."""
    task = models.ForeignKey(
        PracticeTask,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Задача"
    )

    text = models.TextField(
        verbose_name="Текст вопроса",
    )

    def __str__(self):
        return f'{self.text}'

    class Meta:
        verbose_name = "Тестовый вопрос"
        verbose_name_plural = "Тестовые вопросы"


class QuestionTextTask(models.Model):
    """Ответ с свободным выбором."""

    task = models.ForeignKey(
        PracticeTask,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Задача"
    )

    text = models.TextField(
        verbose_name="Текст вопроса",
    )

    def __str__(self):
        return f'{self.text}'

    class Meta:
        verbose_name = "Текстовый вопрос"
        verbose_name_plural = "Текстовый вопрос"


class QuestionFile(models.Model):
    """Вопрос на который отвечают файлом"""

    task = models.ForeignKey(
        PracticeTask,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Практическое задание"
    )

    text = models.TextField(
        verbose_name="Текст вопроса",
    )

    def __str__(self):
        return f'{self.text}'

    class Meta:
        verbose_name = "Файловый вопрос (ответ на них файл ученика)"
        verbose_name_plural = "Файловые вопросы (ответ на них файл ученика)"


class AnswerTaskLinked(models.Model):
    """Ответы на вопросы"""
    task = models.ForeignKey(
        PracticeTask,
        verbose_name="Задание",
        on_delete=models.SET_NULL,
        null=True
    )

    course = models.ForeignKey(
        Course,
        verbose_name="Курсы",
        on_delete=models.SET_NULL,
        null=True
    )

    question = models.ForeignKey(
        QuestionTestTask,
        verbose_name="Вопрос",
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return f'Курс: {self.course} | Ответы по заданию: {self.task} | Вопрос: {self.question}'

    class Meta:
        verbose_name = "Варианты ответов"
        verbose_name_plural = "Варианты ответов"


class AnswerText(models.Model):
    """Текст ответов."""

    linked = models.ForeignKey(
        AnswerTaskLinked,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name="Текст вопроса"
    )

    text = models.TextField(
        verbose_name="Текст ответа",
    )

    is_correct = models.BooleanField(
        verbose_name="Правильный ответ",
        default=False
    )

    class Meta:
        verbose_name = "Вариант ответа"
        verbose_name_plural = "Варианты ответа"


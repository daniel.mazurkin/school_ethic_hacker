from django.contrib import admin
from tasks.models import (
    PracticeTask, QuestionTestTask, AnswerText,
    QuestionFile, AnswerTaskLinked, QuestionTextTask,
)


class QuestionTestTaskInline(admin.TabularInline):
    model = QuestionTestTask
    extra = 1
    inlines = ()


class QuestionFileInline(admin.TabularInline):
    model = QuestionFile
    extra = 1
    inlines = ()


class AnswerTextInline(admin.TabularInline):
    model = AnswerText
    extra = 1


@admin.register(AnswerTaskLinked)
class AnswerTaskAdmin(admin.ModelAdmin):
    inlines = [AnswerTextInline]
    list_filter = ['task', 'course']


class QuestionTextTaskInline(admin.TabularInline):
    model = QuestionTextTask
    extra = 1


@admin.register(PracticeTask)
class PracticeTaskAdmin(admin.ModelAdmin):
    inlines = [QuestionTextTaskInline, QuestionTestTaskInline, QuestionFileInline]
    list_filter = ['course']


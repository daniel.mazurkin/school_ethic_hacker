from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils import timezone
from profile_info.utils import timedelta_to_rus_string
from login_app.forms import EditUserForm
from django.http import HttpResponseRedirect
from users.models import UserModel


@login_required
def get_profile_info(request):
    timedelta_joined = timezone.now() - request.user.date_joined
    timedelta_joined_rus = timedelta_to_rus_string(timedelta_joined)
    return render(request, 'page/profile.html', {
        "username": request.user.username,
        "email": request.user.email,
        "number_phone": request.user.phone,
        "timedelta_joined_rus": timedelta_joined_rus
    })


@login_required
def get_edit_profile_info(request):
    form_edit = EditUserForm()
    return render(request, "page/profile_edit.html", {
        "user": request.user,
        "edit_form": form_edit
    })


@login_required
def edit_profile_info(request):
    user_instance = UserModel.objects.filter(pk=request.user.pk).first()
    user_instance.email = request.POST['email']
    user_instance.phone = request.POST['phone_number']
    user_instance.username = request.POST['username']
    user_instance.save()
    return HttpResponseRedirect(reverse_lazy("profile_edit"))
from datetime import timedelta


def timedelta_to_rus_string(td):
    total_seconds = int(td.total_seconds())
    days = total_seconds // (24 * 3600)
    hours = (total_seconds % (24 * 3600)) // 3600
    minutes = (total_seconds % 3600) // 60
    seconds = total_seconds % 60

    parts = []
    if days:
        parts.append(f"{days} {'день' if days == 1 else 'дня' if days < 5 else 'дней'}")
    if hours:
        parts.append(f"{hours} {'час' if hours == 1 else 'часа' if hours < 5 else 'часов'}")
    if minutes:
        parts.append(f"{minutes} {'минута' if minutes == 1 else 'минуты' if minutes < 5 else 'минут'}")
    if seconds:
        parts.append(f"{seconds} {'секунда' if seconds == 1 else 'секунды' if seconds < 5 else 'секунд'}")

    return ', '.join(parts) if parts else '0 секунд'
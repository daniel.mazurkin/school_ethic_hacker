from django.urls import path
# from courses.views import CourseDetailView

from django.views.generic.base import TemplateView
from . import views


urlpatterns = [
    # path("course/<int:pk>/", CourseDetailView.as_view()),
    path('profile', views.get_profile_info, name="profile"),
    path('profile/edit', views.get_edit_profile_info, name="profile_edit"),
    path('profile/action/edit/', views.edit_profile_info, name='profile_edit_action')
]

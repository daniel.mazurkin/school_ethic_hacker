import os

bind = '0.0.0.0:8000'
workers = os.getenv('GUNICORN_WORKERS', 1)
reload = True

#!/bin/sh

python "manage.py" collectstatic --noinput

python "manage.py" migrate --noinput

gunicorn -c "$GUNICORN_CONF" school_hacker.wsgi:application

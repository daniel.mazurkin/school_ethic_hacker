from django.db import models
from users.models import UserModel


class AchievementUser(models.Model):

    user = models.ForeignKey(
        UserModel, verbose_name="Пользователь",
        on_delete=models.CASCADE
    )

    text = models.CharField(
        max_length=1024, verbose_name="Название достижения",
    )

    def __str__(self):
        return f'{self.user} | {self.text}'

    class Meta:
        verbose_name = "Достижение пользователя"
        verbose_name_plural = "Достижения пользователя"

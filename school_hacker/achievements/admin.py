from django.contrib import admin
from achievements.models import AchievementUser


@admin.register(AchievementUser)
class AchievementUserAdmin(admin.ModelAdmin):
    readonly_fields = ('user', 'text')

    def has_add_permission(self, request):
        return False

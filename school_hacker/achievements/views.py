from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from achievements.models import AchievementUser


@login_required
def achievements_list(request):
    achievements = AchievementUser.objects.filter(user=request.user)
    return render(request, 'page/course_achivment_list.html', {
        "achivies": achievements
    })

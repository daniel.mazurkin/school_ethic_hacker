from django.contrib import admin
from applications.models import ApplicationCourse


@admin.register(ApplicationCourse)
class ApplicationCourseAdmin(admin.ModelAdmin):
    """Регистрация заявки в админке."""

    list_display = ('user', 'course', 'display_user_email', 'display_user_phone', "is_success_payment")
    fieldsets = (
            (None, {
                    'fields': ("user", "course", "display_user_email", "display_user_phone", "is_success_payment")
            }),
    )

    def display_user_email(self, obj):
        return obj.user.email

    def display_user_phone(self, obj):
        return obj.user.phone

    readonly_fields = ("display_user_email", "display_user_phone")
    display_user_email.short_description = "Эл. почта"
    display_user_phone.short_description = "Номер телефона"

    def has_add_permission(self, request):
        return False

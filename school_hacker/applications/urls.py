from django.urls import path
from applications.views import create_application, get_applications


urlpatterns = [
    path("create_application/<int:course_id>", create_application, name='create_application'),
    path("applications", get_applications, name='get_applications')
]


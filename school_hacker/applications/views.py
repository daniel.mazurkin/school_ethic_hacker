from django.shortcuts import redirect, render
from applications.models import ApplicationCourse, Course
from django.http import Http404


def create_application(request, course_id):
    """Создает заявку на курс"""
    if not request.user.is_authenticated:
        return redirect('login')
    
    course = Course.objects.filter(pk=course_id).first()
    
    if not course:
        raise Http404
    
    application = ApplicationCourse.objects.filter(
        user=request.user, course=course,
    ).first()
    msg = None
    is_exists = False
    
    if application and application.is_success_payment:
        is_exists = True
        msg = "Уже установелена заявка на этот курс и оплата принята."
    elif application and not application.is_success_payment:
        is_exists = True
        msg = "Уже установлена заявка на этот курс и ожидается одобрение оплаты"
    else:
        msg = "Ваша заявка на курс была создана"

    if not is_exists:
        application_course = ApplicationCourse(
            user=request.user,
            course=course,
        )
        application_course.save()

    return render(request, "after_application.html", {"msg": msg})


def get_applications(request):
    """Возвращает все заявки пользователя"""
    applications = ApplicationCourse.objects.filter(user=request.user)
    print("applications: ", applications)
    msg_applications = None
    if not applications:
        msg_applications = 'У вас нет созданных заявок'
    return render(request, "applications.html", {
        "applications": applications, "msg_applications": msg_applications
    })
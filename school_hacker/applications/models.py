from django.db import models
from courses.models import Course
from users.models import UserModel


class ApplicationCourse(models.Model):
    """Модель заявка на курс. """

    course = models.ForeignKey(
        Course,
        verbose_name="Курс",
        on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        UserModel,
        verbose_name="Пользователь",
        on_delete=models.CASCADE,
        null=True
    )

    is_success_payment = models.BooleanField(
        default=False,
        verbose_name="Успешная ли оплата"
    )

    def __str__(self):
        return f'Курс: {self.course} | Пользователь: {self.user}'

    class Meta:
        verbose_name = "Заявка на курс"
        verbose_name_plural = "Заявки на курсы"

    @classmethod
    def course_by_data(cls, course, user_course):
        course = cls.objects.filter(course=course, user=user_course).first()
        return course


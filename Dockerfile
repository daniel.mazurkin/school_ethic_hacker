FROM python:3.10-alpine

RUN mkdir /static
WORKDIR /static

COPY ./school_hacker /static

ENV SRC_DIR .
ENV PYTHONPATH $SRC_DIR/school_hacker

COPY . $SRC_DIR

WORKDIR $SRC_DIR

RUN pip install -r requirements.txt
RUN chmod +x "./school_hacker/run_django.sh"
CMD ["sh", "-c", "./school_hacker/run_django.sh"]
